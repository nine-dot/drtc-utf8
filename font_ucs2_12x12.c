#include <ft2build.h>
#include FT_FREETYPE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <png.h>
#include <GL/glut.h>
#include <png.h>
#include <stdio.h>

#define WIDTH 3072
#define HEIGHT 3072
GLuint textureID;
GLubyte textureData[WIDTH][HEIGHT][4];

void saveTextureToPNG(const char* filename, int width, int height) {
    FILE *fp = fopen(filename, "wb");
    if(!fp) {
        fprintf(stderr, "Cannot open %s for writing\n", filename);
        return;
    }

    png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    png_infop info_ptr = png_create_info_struct(png_ptr);

    png_init_io(png_ptr, fp);
    png_set_IHDR(png_ptr, info_ptr, width, height, 8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
    png_write_info(png_ptr, info_ptr);

    for(int y = 0; y < height; y++) {
        png_write_row(png_ptr, &textureData[y][0][0]);
    }

    png_write_end(png_ptr, NULL);
    png_destroy_write_struct(&png_ptr, &info_ptr);

    fclose(fp);
}

void display() {
    glClear(GL_COLOR_BUFFER_BIT);

    glBindTexture(GL_TEXTURE_2D, textureID);
    glBegin(GL_QUADS);
    glTexCoord2f(0.0, 0.0); glVertex2f(-0.5, -0.5);
    glTexCoord2f(1.0, 0.0); glVertex2f(0.5, -0.5);
    glTexCoord2f(1.0, 1.0); glVertex2f(0.5, 0.5);
    glTexCoord2f(0.0, 1.0); glVertex2f(-0.5, 0.5);
    glEnd();

    glutSwapBuffers();

    // Capture the texture data
    glBindTexture(GL_TEXTURE_2D, textureID);
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, textureData);

    // Save the texture to a PNG
    saveTextureToPNG("D:/program/Steam/steamapps/common/DeathRoadToCanada/gfx/fonts/font_ucs2_12x12.png", WIDTH, HEIGHT);

    // End the program after saving the image
    exit(0);
}


GLuint textureID;
int atlas_width = 3072;
int atlas_height = 3072;

typedef struct {
    unsigned char r, g, b, a;
} RGBA;

typedef struct {
    int width, height;
    RGBA* data;
} Texture2D;

Texture2D getTextureData(GLuint textureId, int width, int height) {
    Texture2D texture;
    texture.width = width;
    texture.height = height;
    texture.data = (RGBA*)malloc(width * height * sizeof(RGBA));

    // 绑定纹理，以便我们可以读取它
    glBindTexture(GL_TEXTURE_2D, textureId);

    // 获取纹理数据
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, texture.data);

    // 释放绑定
    glBindTexture(GL_TEXTURE_2D, 0);

    return texture;
}

void copyTextureSubData(Texture2D* srcTexture, Texture2D* dstTexture, int srcX, int srcY, int dstX, int dstY, int width, int height) {
    for (int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            RGBA srcPixel = srcTexture->data[(srcY + y) * srcTexture->width + (srcX + x)];
            dstTexture->data[(dstY + y) * dstTexture->width + (dstX + x)] = srcPixel;
        }
    }
}

void font_set()
{
    FT_Error err_ft;


    // 初始化FreeType
    FT_Library ft;
    FT_Init_FreeType(&ft);
    FT_Face face;
    err_ft = FT_New_Face(ft, ".\\Zpix.ttf", 0, &face);
    if (err_ft) {
        // FT_New_Face失败
        if (err_ft == FT_Err_Unknown_File_Format) {
            // 字体文件打开了，但格式不被支持
            printf("steam_wrapper error: Zpix.ttf file format error");
        } else {
            // 字体文件无法打开或读取，或者是无效的
            printf("steam_wrapper error: Zpix.ttf not find");
        }
    }
    FT_Set_Pixel_Sizes(face, 12, 12);

    Texture2D dstTexture;
    dstTexture.width = atlas_width;
    dstTexture.height = atlas_height;
    dstTexture.data = (RGBA*)calloc(dstTexture.width * dstTexture.height,sizeof(RGBA));


//    sprintf(msg,"OpenGL info %04X", glGetError());
//    MessageBox(NULL, msg, "", MB_OK);

    FT_Bitmap* bitmap;
    int width;
    int height;
    int top;
    int left;
    GLubyte* buffer;

//    sprintf(msg,"steam_wrapper info: font init start w:%d h:%d",atlas_width,atlas_height);
//    MessageBox(NULL, msg, "", MB_OK);
    int count = 0;
    for(int i=0x0000;i<0xFFFF;i++){
        FT_Load_Char(face, i, FT_LOAD_RENDER);

        bitmap = &face->glyph->bitmap;
        width = bitmap->width;
        height = bitmap->rows;
        int baseline = face->size->metrics.ascender >> 6;
        top = face->glyph->bitmap_top;
        left = face->glyph->bitmap_left;
        int size = width * height * 4;
        buffer = (GLubyte *) malloc(size);
        for (int y = 0; y < height; y++)
        {
            for (int x = 0; x < width; x++)
            {
                int grayscale_value = bitmap->buffer[y * width + x];
                buffer[(y * width + x) * 4 + 0] = grayscale_value; // Red
                buffer[(y * width + x) * 4 + 1] = grayscale_value; // Green
                buffer[(y * width + x) * 4 + 2] = grayscale_value; // Blue
                buffer[(y * width + x) * 4 + 3] = grayscale_value; // Alpha
            }
        }

        GLuint texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

        glBindTexture(GL_TEXTURE_2D, textureID);
        int c_offset = i;
        int y_offset = c_offset / 256 * 12;
        int x_offset = c_offset % 256 * 12;

        int top_offset = baseline-top-1;

        Texture2D ftTexture = getTextureData(texture,width,height);
        copyTextureSubData(&ftTexture,&dstTexture,0,0,x_offset,y_offset+top_offset,width, height);

        glDeleteTextures(1, &texture);
        free(buffer);
    }
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, atlas_width, atlas_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, dstTexture.data);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

    FT_Done_Face(face);
    FT_Done_FreeType(ft);
}

int main(int argc, char** argv) {
    glutInit(&argc, argv);
    glutInitWindowSize(WIDTH, HEIGHT);
    glutCreateWindow("Save Texture as PNG");

    font_set();

    glutDisplayFunc(display);
    glutMainLoop();

    return 0;
}
